﻿CREATE TABLE [dbo].[Shipment] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [Number]        NVARCHAR (13) NULL,
    [OutcomeDate]   DATETIME      NOT NULL,
    [IsConsignment] BIT           NOT NULL,
    [CreateOn]      DATETIME      NOT NULL,
    CONSTRAINT [PK_Shipment] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨單號-S201811260001', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Shipment', @level2type = N'COLUMN', @level2name = N'Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Shipment', @level2type = N'COLUMN', @level2name = N'OutcomeDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否寄賣', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Shipment', @level2type = N'COLUMN', @level2name = N'IsConsignment';

