﻿CREATE TABLE [dbo].[ReciveDetail] (
    [Id]             INT  IDENTITY (1, 1) NOT NULL,
    [ReciveId]       INT  NOT NULL,
    [ProductId]      INT  NOT NULL,
    [Qty]            INT  NULL,
    [ExpirationDate] DATE NULL,
    CONSTRAINT [PK_ReciveDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ReciveDetail_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id]),
    CONSTRAINT [FK_ReciveDetail_Recive] FOREIGN KEY ([ReciveId]) REFERENCES [dbo].[Recive] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'數量', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ReciveDetail', @level2type = N'COLUMN', @level2name = N'Qty';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'有效日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ReciveDetail', @level2type = N'COLUMN', @level2name = N'ExpirationDate';

