﻿CREATE TABLE [dbo].[Stock] (
    [Id]             INT NOT NULL,
    [ProductId]      INT NOT NULL,
    [InitStock]      INT NOT NULL,
    [StockIn]        INT NOT NULL,
    [StockOut]       INT NOT NULL,
    [ConsignmentIn]  INT NOT NULL,
    [ConsignmentOut] INT NOT NULL,
    CONSTRAINT [PK_Stock] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Stock_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id])
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'庫存', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Stock', @level2type = N'COLUMN', @level2name = N'Id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'期初', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Stock', @level2type = N'COLUMN', @level2name = N'InitStock';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進貨數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Stock', @level2type = N'COLUMN', @level2name = N'StockIn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Stock', @level2type = N'COLUMN', @level2name = N'StockOut';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄賣進貨', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Stock', @level2type = N'COLUMN', @level2name = N'ConsignmentIn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄賣出貨', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Stock', @level2type = N'COLUMN', @level2name = N'ConsignmentOut';

