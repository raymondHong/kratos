﻿CREATE TABLE [dbo].[Recive] (
    [Id]            INT           IDENTITY (1, 1) NOT NULL,
    [Number]        NVARCHAR (13) NULL,
    [IncomeDate]    DATETIME      NOT NULL,
    [IsConsignment] BIT           NOT NULL,
    [CreateOn]      DATETIME      NOT NULL,
    CONSTRAINT [PK_Recive] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進貨單號-R201811260001', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Recive', @level2type = N'COLUMN', @level2name = N'Number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進貨日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Recive', @level2type = N'COLUMN', @level2name = N'IncomeDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否寄賣', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Recive', @level2type = N'COLUMN', @level2name = N'IsConsignment';

