﻿CREATE TABLE [dbo].[ShipmentDetail] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [ShipmentId] INT NOT NULL,
    [ProductId]  INT NOT NULL,
    [Qty]        INT NOT NULL,
    CONSTRAINT [PK_ShipmentDetail] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ShipmentDetail_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([Id]),
    CONSTRAINT [FK_ShipmentDetail_Shipment] FOREIGN KEY ([ShipmentId]) REFERENCES [dbo].[Shipment] ([Id])
);

