﻿const productService = (function() {
    const service = {
        get: get,
        add: add,
        remove: remove,
        edit: edit
    };

    function get() {
        return $.get(`/Api/Products/GetProduct`);
    }

    function add(postData) {
        return $.post(`/Api/Products/PostProduct`, postData);
    }

    function edit(postData) {
        return $.post(`/Api/Products/PutProduct`, postData);
    }

    function remove(id) {
        return $.get(`/Api/Products/DeleteProduct?id=${id}`);
    }

    return service;
})();